# Demo for SonarSource Webinar

Only the master branch and Merge Requests are analyzed.

# DEMO: Code Quality & Security in your ALM

This project branch is used to demonstrate SonarQube Merge Request and branch decoration of Code Quality and Security metrics including the Sonar Way Quality Gate.

## Code issues presented in this project

### Code Smell

* https://rules.sonarsource.com/java/RSPEC-1120 - Source code should be indented consistently

### Security Hotspot

* https://rules.sonarsource.com/java/RSPEC-4502 - Disabling CSRF protections is security-sensitive
